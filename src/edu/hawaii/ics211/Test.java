package edu.hawaii.ics211;

/**
 * 
 * @author Jason Leong
 * Testing all methods from Calculator class (add, subtract, multiply, divide, modulo, pow)
 */
public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calculator calculator = new Calculator();
		System.out.println(new Calculator().add(1, 2)); //Should return 3
		System.out.println(new Calculator().subtract(4, 1)); //Should return 3
		System.out.println(new Calculator().multiply(3, 1)); //Should return 3
		System.out.println(new Calculator().divide(6, 2)); //Should return 3
		System.out.println(new Calculator().modulo(19, 4)); //Should return 3
		System.out.println(new Calculator().pow(3, 1)); //Should return 3
	}

	/**
	 * Tests divide by 0 exception for division method. 
	 */
	public void testDivision() {
		Calculator calc = new Calculator();
		try {	
			calc.divide(5, 0);
		}
		catch (ArithmeticException e) {
		}
	}
	
}






